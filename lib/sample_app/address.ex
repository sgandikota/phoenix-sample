defmodule SampleApp.Address do
    use Ecto.Schema
    import Ecto.Changeset
  
    schema "addresses" do
      field :line1, :string
      field :line2, :string
      field :country, :string
      field :city, :string
      field :state, :string
      belongs_to :user, SampleApp.User
  
      timestamps()
    end
  
    @doc false
    def changeset(user, attrs) do
      user
      |> cast(attrs, [:line1, :line2, :country, :city, :state, :user_id])
      |> validate_required([:line1, :country, :state, :user_id])
    end
  end
  