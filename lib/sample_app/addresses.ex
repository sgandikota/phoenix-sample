defmodule SampleApp.Addresses do
    alias SampleApp.{Repo,Address}
    import Ecto.Query


    def get_address(user_id) do
        query = from a in Address, where: a.user_id == ^user_id
        Repo.all(query)
    end

    def create_address(address) do
        %Address{} 
        |> Address.changeset(address)
        |> Repo.insert
    end

end