defmodule SampleApp.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, EctoHashedPassword

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :email, :password])
    |> validate_required([:name, :email, :password])
    |> unique_constraint(:email)
  end


  defp encrypt_password(changeset) do
    password = get_change(changeset, :password)
    if password do
      encrypted_password = SampleApp.Users.hash_password(password)
      put_change(changeset, :password, encrypted_password)
    else
      changeset
    end
  end
end
