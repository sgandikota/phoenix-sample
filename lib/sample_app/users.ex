defmodule SampleApp.Users do
    alias SampleApp.{Repo,User}
    alias Comeonin.Bcrypt
    import Ecto.Query

    def find_user(email, password) do
        # query = from u in "users", 
        #     where: [email: ^email, password: ^password],
        #     select: [:id, :name]

        # case Repo.one(query) do
        #     %{id: id, name: name} = user -> {:ok, user}
        #     nil -> {:error, :unauthorized}
        # end
        user = Repo.get_by(User, email: email)
        case EctoHashedPassword.checkpw(password, user.password) do
            true -> {:ok, user}
            _    -> {:error, :unauthorized}
          end
    end

    defp authenticate(user, password) do
        if user do
          {:ok, authenticated_user} = validate_password(user, password)
          authenticated_user.email == user.email
        else
          nil
        end
      end

    def get_user(id) do
        User
        |> Repo.get(id)
    end

    def get_all() do
        User
        |> Repo.all
    end

    def create_user(user) do
        %User{} 
        |> User.changeset(user)
        |> Repo.insert
    end

    def update_user(user, params) do
      user
      |> User.changeset(params)
      |> Repo.update
    end


    def hash_password(password), do: Bcrypt.hashpwsalt(password)

    def validate_password(%User{} = user, password), do: Bcrypt.check_pass(user, password)
end