defmodule SampleAppWeb.UserController do 
    use SampleAppWeb, :controller
    alias SampleApp.Users

    def list(conn, _params) do
        users = SampleApp.Users.get_all()
        conn
        |> put_status(:ok)
        |> render("users.json", %{users: users})
    end

    def get_by_id(conn, %{"id" => id}) do
        user = Users.get_user(id)
        if user do
            conn |> render("user.json", %{user: user})
        else
            json conn |> put_status(:not_found),
                   %{errors: ["invalid user"] }
        end
    end

    def update(conn, %{"id" => id, "name" => name} = params) do
        user = Users.get_user(id)
        if user do
          case Users.update_user(user, %{"name" => name}) do
            {:ok, user} -> conn |> render("user.json", %{user: user})
            {:error, _} -> json conn |> put_status(:bad_request), %{errors: ["Bad update"]}
          end
        else
            json conn |> put_status(:not_found),
                   %{errors: ["invalid user"] }
        end
    end
end