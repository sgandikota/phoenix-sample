defmodule SampleAppWeb.AuthController do
    use SampleAppWeb, :controller
    alias SampleApp.{Repo,User}
    import Ecto.Query
    
    def login(conn, _params) do
        render(conn, "login.html")
    end

    def new(conn, _params) do
        render(conn, "new.html")
    end

    def create(conn, params) do
        case  SampleApp.Users.create_user(params) do
            {:error, changeset} ->
                errors = Ecto.Changeset.traverse_errors(changeset, fn
                    {msg, opts} -> String.replace(msg, "%{count}", 
                        to_string(opts[:count]))
                    msg -> msg
                  end)
                conn
                |> json(errors)
            {:ok, %User{} = user} -> 
                conn
                |> put_status(:created)
                |> render("user.json",  %{user: user})
        end
    end

    def signin(conn, %{"email" => email, "password" => password}) do
        case SampleApp.Users.find_user(email, password) do
            {:ok, user} ->
                conn
                |> put_flash(:info, "Welcome back!")
                |> put_session(:user_id, user.id)
                |> configure_session(renew: true)
                |> redirect(to: "/pages/home_page")
            {:error, :unauthorized} ->
                conn
                |> put_flash(:error, "Login failed")
                |> redirect(to: "/login")

        end
    end

    def logout(conn, _) do
        conn 
        |> configure_session(drop: true)
        |> redirect(to: "/login")
    end
end