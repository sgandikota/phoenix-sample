defmodule SampleAppWeb.AddressController do
    use SampleAppWeb, :controller
    alias SampleApp.{Address}

    def get_by_user(conn, %{"id" => id}) do
        IO.puts id
        addresses = SampleApp.Addresses.get_address(id)
        IO.puts inspect addresses
        conn
        |> put_status(:ok)
        |> render("addresses.json", %{addresses: addresses})
    end

    def create(conn, params) do
       case SampleApp.Addresses.create_address(params) do
        {:error, changeset} ->
            errors = Ecto.Changeset.traverse_errors(changeset, fn
                {msg, opts} -> String.replace(msg, "%{count}", 
                    to_string(opts[:count]))
                msg -> msg
              end)
            conn
            |> json(errors)
        {:ok, %Address{} = address} -> 
            conn
            |> put_status(:created)
            |> render("address.json",  %{address: address})
        end
    end
end