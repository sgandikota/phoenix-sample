defmodule SampleAppWeb.Router do
  use SampleAppWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :fetch_flash
  end

  scope "/", SampleAppWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/login", AuthController, :login
    get "/logout", AuthController, :logout
    get "/users/new", AuthController, :new
    
    
  end

  scope "/pages", SampleAppWeb do
    pipe_through [:browser, :authenticate_user]

    get "/home_page", HomeController, :index
    get "/users/list", AuthController, :list
  end

  scope "/api", SampleAppWeb do
    pipe_through :api
    post "/login", AuthController, :signin
    get  "/users", UserController, :list
    post "/users", AuthController, :create
    put  "/users/:id", UserController, :update
    get  "/users/:id", UserController, :get_by_id
    get  "/users/:id/address", AddressController, :get_by_user
    post "/addresses", AddressController, :create
  end


  defp authenticate_user(conn, _) do
    case get_session(conn, :user_id) do
      nil ->
        conn
        |> Phoenix.Controller.put_flash(:error, "Login required")
        |> Phoenix.Controller.redirect(to: "/login")
        |> halt()
      user_id ->
        assign(conn, :current_user, SampleApp.Users.get_user(user_id))
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", SampleAppWeb do
  #   pipe_through :api
  # end
end
