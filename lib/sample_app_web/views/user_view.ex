defmodule SampleAppWeb.UserView do
    use SampleAppWeb, :view

    alias SampleAppWeb.UserView

    def render("users.json", %{users: users}) do
        render_many(users, UserView, "user.json")
    end
    def render("user.json", %{user: user}) do
        %{ id: user.id,
           name: user.name,
           email: user.email}
    end
end