defmodule SampleAppWeb.AddressView do
    use SampleAppWeb, :view

    def render("addresses.json", %{addresses: addresses}) do
        render_many(addresses, SampleAppWeb.AddressView, "address.json")
    end
    def render("address.json", %{address: address}) do
        %{ id: address.id,
           line1: address.line1,
           line2: address.line2}
    end
end