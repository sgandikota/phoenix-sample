defmodule SampleAppWeb.AuthView do
    use SampleAppWeb, :view

    alias SampleAppWeb.UserView

    def render("user.json", %{user: user}) do
      render_one(user, UserView, "user.json")
    end
end