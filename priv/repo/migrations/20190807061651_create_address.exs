defmodule SampleApp.Repo.Migrations.CreateAddress do
  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :line1, :string
      add :line2, :string
      add :couuntry, :string
      add :state, :string
      add :city, :string
      add :user_id, references(:users)
    end
  end
end
