defmodule SampleApp.Repo.Migrations.AddTimestampsAddress do
  use Ecto.Migration

  def change do
    alter table(:addresses) do
      timestamps()
    end
  end
end
